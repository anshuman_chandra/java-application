# Java Application

CREATE DATABASE `anzTest`;

CREATE TABLE `users` (
	`ID` INT(5) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(20) NOT NULL,
	`surname` VARCHAR(20) NOT NULL,
	`firstname` VARCHAR(20) NOT NULL,
	`password` VARCHAR(200) NOT NULL
);


CREATE TABLE `accounts` (
	`id` INT(20) NOT NULL AUTO_INCREMENT,
	`user_id` INT(5) NOT NULL,
	`account_name` VARCHAR(50) NOT NULL,
	`account_number` BIGINT(30) NOT NULL,
	`account_type` VARCHAR(20) NOT NULL,
	`account_balance` BIGINT(20),
	`balance_date` DATE,
	`currency` VARCHAR(10)
);

CREATE TABLE `transactions` (
	`id` INT(20) NOT NULL primary key AUTO_INCREMENT,
	`account_id` INT(5) NOT NULL,
	`transaction_description` VARCHAR(200) NOT NULL,
	`transaction_amount` INT(30) NOT NULL,
	`transaction_type` VARCHAR(20),
	`value_date` DATE
)ENGINE=InnoDB DEFAULT CHARSET=latin1;




INSERT INTO `accounts`(user_id,account_name,account_number,account_type,account_balance,balance_date,currency) 
VALUES 
(1,'SGCurrent726','894499474','Current', 2272.07,CURDATE(), 'SCG'),
(1,'AUDCredit','779393939','Credit', 9989.51,CURDATE(), 'AUD'),
(1,'SGCurrent888','992929292','Current', 7383.88,CURDATE(), 'AUD'),
(1,'SGSavings999','778866776','Savings', 93923.33,CURDATE(), 'AUD'),
(1,'USDSavings','2233737383','Savings', 7838.23,CURDATE(), 'AUD'),
(1,'USDCurrent','3939489439','Current', 2233.77,CURDATE(), 'AUD'),
(1,'USDCredit','884893489','Credit', 1909.43,CURDATE(), 'USD'),
(1,'AUDCredit','883935755','Credit', 6738.09,CURDATE(), 'AUD'),
(1,'AUDCurrent','9098099292','Current', 8937.12,CURDATE(), 'AUD'),
(1,'SCGCredit','8383938489','Credit', 6738.67,CURDATE(), 'SCG');



INSERT INTO `users`(username,surname,firstname,password) VALUES 
('testusername','TestLastName','TestFirstName','Ivq9xh0CUN1TGlLU2S/Tq89N7UkdDiuImTD3Ky6XkBw='),
('testusername1','TestLastName1','TestFirstName1','Ivq9xh0CUN1TGlLU2S/Tq89N7UkdDiuImTD3Ky6XkBw='),
('testusername2','TestLastName2','TestFirstName2','Ivq9xh0CUN1TGlLU2S/Tq89N7UkdDiuImTD3Ky6XkBw='),

INSERT INTO `transactions`(account_id,transaction_description,transaction_amount,transaction_type,value_date) 
VALUES 
(1,'Groceries Shopping',200.09,'Debit',CURDATE()),
(1,'Salary',4000.99, 'Credit',CURDATE()),
(1,'Telephone Bill',49.00,'Debit',CURDATE()),
(1,'Transport',50,'Debit',CURDATE()),
(1,'Salary',40000,'Credit',CURDATE()),
(1,'Movie',20,'Debit',CURDATE()),
(1,'Salary',40000,'Credit',CURDATE()),
(1,'Transfer',500,'Debit',CURDATE()),
(1,'Transfer',700,'Credit',CURDATE()),
(1,'Direct Debit cash',8000,'Credit',CURDATE());
