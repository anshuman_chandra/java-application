package com.anz.test;

import com.anz.test.Service.UserService;
import com.anz.test.entity.Users;
import com.anz.test.repository.userJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = { PersistenceTestConfig.class }, loader = AnnotationConfigContextLoader.class)
@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@DataJpaTest
public class UserServiceTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private userJpaRepository userRepo;

    @Autowired
    private UserService userService;

    @Test
    public void create_should_create_a_new_entity() {
        final Users users = new Users();
        users.setId(1L);
        users.setFirstname("TestName");
        users.setSurname("Surname");
        users.setUsername("testusername");
        userRepo.save(users);
        userRepo.flush();
        Optional<Users> actualUser = userRepo.findById(1L);
        actualUser.ifPresent(usr -> {
            assertEquals(usr.getFirstname(),users.getFirstname());
                });
    }

    @Test
    public void userServiceTest()
    {
        final Users users = new Users();
        users.setId(2L);
        users.setFirstname("TestName");
        users.setSurname("Surname");
        users.setUsername("testusername");
        userRepo.save(users);
        userRepo.flush();
        Users actualUser = userService.getUser(2L);

        assertEquals(actualUser.getFirstname(),users.getFirstname());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void invalidUserTest()
    {
        final Users users = new Users();
        users.setId(1234L);
        users.setFirstname("TestName");
        users.setSurname("Surname");
        users.setUsername("testusername");
        userRepo.save(users);
        userRepo.flush();
        Users actualUser = userService.getUser(100L);

    }
}
