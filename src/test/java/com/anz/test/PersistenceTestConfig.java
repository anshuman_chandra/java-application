package com.anz.test;

import com.anz.test.Service.AccountService;
import com.anz.test.Service.TransactionsService;
import com.anz.test.Service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceTestConfig {

    @Bean
    public UserService userService()
    {
        return new UserService();
    }

    @Bean
    public AccountService accountService() {return new AccountService();}

    @Bean
    public TransactionsService transactionsService() {return new TransactionsService();}
}
