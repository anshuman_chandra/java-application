package com.anz.test.controller;

import java.util.List;
        import java.util.Optional;

        import com.anz.test.Service.UserService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.MediaType;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.*;

        import com.anz.test.entity.Users;

@Controller
@RequestMapping("/")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/")
    public String findAll(Model model) {

        List<Users> users = userService.findAll();
        model.addAttribute("usersList", users);
        return "index";
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getUser(@PathVariable Long id, Model model){
        Users user =  userService.getUser(id);
        model.addAttribute("UserData", user);
        return "UserAccount";
    }
}