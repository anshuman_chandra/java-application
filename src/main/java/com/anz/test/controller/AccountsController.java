package com.anz.test.controller;

import com.anz.test.Service.AccountService;
import com.anz.test.entity.Accounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/account")
public class AccountsController {

    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/{userId}")
    public String getAccountsListByUser(@PathVariable("userId") Long userId, Model model)
    {
       List<Accounts> accounts = accountService.getAccoutsByUser(userId);
       model.addAttribute("accountsList", accounts);
       return "accounts";

    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Optional<Accounts> getAccountTransactions(@PathVariable Long id){
        return accountService.findById(id);
    }
}
