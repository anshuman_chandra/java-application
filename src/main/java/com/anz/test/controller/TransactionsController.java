package com.anz.test.controller;

import com.anz.test.Service.TransactionsService;
import com.anz.test.entity.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/transaction")
public class TransactionsController {

    @Autowired
    private TransactionsService transactionService;

    @GetMapping(value = "/{accountId}")
    public String getTransactionsListByAccount(@PathVariable("accountId") Long accountId, Model model)
    {
        List<Transactions> transactions = transactionService.getTransByAccount(accountId);
        model.addAttribute("transactionSList", transactions);
        model.addAttribute("accountId", accountId);
        return "transactions";

    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Optional<Transactions> getAccountTransactions(@PathVariable Long id){
        return transactionService.findById(id);
    }

}
