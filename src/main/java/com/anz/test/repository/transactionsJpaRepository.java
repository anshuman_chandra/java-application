package com.anz.test.repository;

import com.anz.test.entity.Accounts;
import com.anz.test.entity.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface transactionsJpaRepository extends JpaRepository<Transactions, Long> {
    List<Transactions> findByAccounts(Accounts accounts);
}
