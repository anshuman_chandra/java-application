package com.anz.test.repository;

import com.anz.test.entity.Accounts;
import com.anz.test.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface accountsJpaRepository extends JpaRepository<Accounts, Long> {
    List<Accounts> findByUser(Users usr);
}
