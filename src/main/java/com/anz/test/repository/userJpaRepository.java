package com.anz.test.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.anz.test.entity.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface userJpaRepository extends JpaRepository<Users, Long> {
}
