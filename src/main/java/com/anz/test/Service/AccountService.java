package com.anz.test.Service;

import com.anz.test.entity.Accounts;
import com.anz.test.entity.Users;
import com.anz.test.repository.accountsJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountService {

    @Autowired
    private accountsJpaRepository accountRepo;

    @Autowired
    private UserService userService;

    public Optional<Accounts> get(Long id){
        return Optional.ofNullable(accountRepo.findById(id).get());
    }

    public Optional<Accounts> findById(Long id) {
        return accountRepo.findById(id);
    }

    public List<Accounts> getAccoutsByUser(Long userId)
    {
        Users user = userService.getUser(userId);
        return accountRepo.findByUser(user);
    }
}
