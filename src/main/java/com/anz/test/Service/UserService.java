package com.anz.test.Service;

import com.anz.test.entity.Users;
import com.anz.test.repository.userJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private userJpaRepository userRepo;

    public List<Users> findAll()
    {
        return userRepo.findAll();
    }

    public Users getUser(Long id){

        return userRepo.findById(id).orElseThrow(()->new ResourceNotFoundException("Testing 123"));
    }

}
