package com.anz.test.Service;

import com.anz.test.entity.Accounts;
import com.anz.test.entity.Transactions;
import com.anz.test.repository.transactionsJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TransactionsService {

    @Autowired
    transactionsJpaRepository transactionRepo;

    @Autowired
    AccountService accountService;

    public Transactions get(Long id){
        return transactionRepo.findById(id).get();
    }

    public Optional<Transactions> findById(Long id) {
        return transactionRepo.findById(id);
    }

    public List<Transactions> getTransByAccount(Long accountId)
    {
        Optional<Accounts> account = accountService.get(accountId);
        return account.map(acc -> transactionRepo.findByAccounts(acc)).orElseThrow(()->new ResourceNotFoundException("Account not found"));
    }
}
